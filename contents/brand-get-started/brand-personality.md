---
name: Brand personality
---

GitLab's brand has a personality that is reflected in everything we do. It doesn't matter if we are hosting a fancy dinner with fortune 500 CIOs, at a hackathon, or telling our story on about.gitlab.com…across all our communication methods, and all our audiences, GitLab has a personality that shows up in how we communicate.

Our personality is built around four main characteristics.

1. Human: We write like we talk. We avoid buzzwords and jargon, and instead communicate simply, clearly, and sincerely. We treat people with kindness.
2. Competent: We are highly accomplished, and we communicate with conviction. We are efficient at everything we do.
3. Quirky: We embrace diversity of opinion. We embrace new ideas based on their merit, even if they defy commonly held norms.
4. Humble: We care about helping those around us achieve great things more than we care about our personal accomplishments.

These four characteristics work together to form a personality that is authentic to GitLab team members, community, and relatable to our audience. If we were **quirky** without being **human** we could come across as eccentric. If we were **competent** without being **humble** we could come across as arrogant.

GitLab has a [higher purpose](https://about.gitlab.com/company/strategy/#mission). We want to inspire a sense of adventure in those around us so that they join us in contributing to making that mission a reality.
