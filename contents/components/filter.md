---
name: Filter
---

Filters allow users to narrow down content by taking an existing list and removing items based on criteria that match or don't match.
